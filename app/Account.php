<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Account extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'type_id',
        'user_id',
    ];

    /**
     * Scope function.
     *
     * Get the debit accounts
     *
     * @param $query
     */
    public function scopeDebit($query){
     	$query->where('type_id', config('constants.ACCOUNT_TYPE_INDEX.debit'))->orWhere('type_id', config('constants.ACCOUNT_TYPE_INDEX.cash'));
     }    
     
    /**
     * Scope function.
     *
     * Get the credit accounts
     *
     * @param $query
     */
    public function scopeCredit($query){
     	$query->where('type_id', config('constants.ACCOUNT_TYPE_INDEX.credit'))->orWhere('type_id', config('constants.ACCOUNT_TYPE_INDEX.debt'));
     }         
    
    /**
     * Returns the type name of the account defined in constants file
     *
     * @return string
     */
    public function getType()
    {
        switch ($this->type_id)
        {
            case config('constants.ACCOUNT_TYPE_INDEX.debit'):
                $account = config('constants.ACCOUNT_TYPE.debit');
                break;
            case config('constants.ACCOUNT_TYPE_INDEX.credit'):
                $account = config('constants.ACCOUNT_TYPE.credit');
                break;
            case config('constants.ACCOUNT_TYPE_INDEX.cash'):
                $account = config('constants.ACCOUNT_TYPE.cash');
                break;
            case config('constants.ACCOUNT_TYPE_INDEX.debt'):
                $account = config('constants.ACCOUNT_TYPE.debt');
                break;
            default:
                $account = null;
                break;
        }

        return $account;
    }

    /**
     * An account belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * An account has many statistics
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statistics()
    {
        return $this->hasMany('App\AccountStatistic');
    }    

    /**
     * An account has many transactions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    /**
     * An account destination (from a transfer) has many transactions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions2()
    {
        return $this->hasMany('App\Transaction', 'account2_id');
    }

    /**
     * Get the logged user's accounts by title.
     *
     * @param String $title
     * @return array
     */
    public static function getAccountsByTitle($title)
    {
        return Auth::user()->accounts()->where('title', 'like', "%".$title."%")->lists('title', 'id');
    }

    /**
     * Get the logged user's accounts by type
     *
     * @param $type_id
     * @return mixed
     */
    public static function getUserAccountsByType($type_id)
    {
        return Auth::user()->accounts()->where('type_id', $type_id)->lists('title', 'id');
    }

    /**
     * Sums all the transactions paid amounts of an account and a given month
     *
     * @param $account_id
     * @param $date
     * @return int
     */
    public static function getTotalAmount($account_id, $date = null)
    {
        if ($date == null)
        {
            $statistics = Auth::user()->accounts()->where('id', $account_id)->first()->statistics()->where('month', '<=', date('Y-m'))->get();
        }
        else
        {
            $statistics = Auth::user()->accounts()->where('id', $account_id)->first()->statistics()->where('month', '<=', $date)->get();
        }

        $total = 0;

        foreach ($statistics as $statistic)
        {
            $total += $statistic->total_amount;
        }    

        return $total;
    }


}