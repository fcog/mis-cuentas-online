<?php

namespace App\Http\Requests;


class TransferRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'amount_paid' => 'required|regex:/^[\d*\.?\d*]+$/',
            'category_id' => 'exists:categories,id',
            'account_id' => 'exists:accounts,id',
            'account2_id' => 'exists:accounts,id',
            'paid_date' => 'date_format:Y-m-d',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El título es obligatorio',
            'title.min' => 'El título está muy corto',
            'amount_paid.required' => 'El monto es obligatorio',
            'amount_paid.regex' => 'El monto debe ser un valor numérico',
            'category_id.exists' => 'La categoría es obligatoria',
            'account_id.exists' => 'La cuenta origen es obligatoria',
            'account2_id.exists' => 'La cuenta destino es obligatoria',
            'paid_date.date_format' => 'La fecha de pago debe tener el formato año-mes-dia'
        ];
    }
}