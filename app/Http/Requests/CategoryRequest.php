<?php

namespace App\Http\Requests;


class CategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'type_id' => 'in:1,2'
        ];
    }

    /**
     * Custom validation error messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'El título es obligatorio',
            'title.min' => 'El título está muy corto',
            'type_id.in' => 'El tipo es obligatorio',
        ];
    }
}
