<?php

namespace App\Http\Requests;


class FixedTransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'amount' => 'required|regex:/^[\d*\.?\d*]+$/',
            'type_id' => 'in:1,2,3',
            'category_id' => 'exists:categories,id',
            'payment_day' => 'numeric|between:1,31',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El título es obligatorio',
            'title.min' => 'El título está muy corto',
            'amount.required' => 'El monto es obligatorio',
            'amount_paid.regex' => 'El monto debe ser un valor numérico',
            'type_id.in' => 'El tipo es obligatorio',
            'category_id.exists' => 'La categoría es obligatoria',
            'payment_day.between' => 'El dia de pago en el mes debe ser un valor entre 1 y 31'
        ];
    }
}
