<?php

namespace App\Http\Requests;


class TransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $paid_date_validation = 'date_format:Y-m-d';
        $account_id_validation = '';

        if(!empty(Request::get('paid_date')))
        {
            $account_id_validation = 'exists:accounts,id';
        } 

        if(!empty(Request::get('account_id')))
        {
            $paid_date_validation .= '|required';
        }     

        return [
            'title' => 'required|min:3',
            'amount_paid' => 'required|regex:/^[\d*\.?\d*]+$/',
            'category_id' => 'exists:categories,id',
            'paid_date' => $paid_date_validation,
            'account_id' => $account_id_validation
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El título es obligatorio',
            'account_id.exists' =>'La cuenta es obligatoria cuando se ingresa una fecha de pago',
            'paid_date.required' =>'La fecha de pago es obligatoria cuando se selecciona una cuenta',
            'title.min' => 'El título está muy corto',
            'amount_paid.required' => 'El monto es obligatorio',
            'amount_paid.regex' => 'El monto debe ser un valor numérico',
            'category_id.exists' => 'La categoría es obligatoria',
            'paid_date.date_format' => 'La fecha de pago debe tener el formato año-mes-dia'
        ];
    }
}
