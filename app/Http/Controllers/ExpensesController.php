<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use App\Http\Requests\TransactionRequest;
use App\Http\Requests;
use App\FixedTransaction;
use App\Transaction;
use App\Category;
use App\AccountStatistic;
use App\GeneralStatistic;


class ExpensesController extends Controller
{

    /**
     * Loads Authentication middleware for all methods
     *
     * FixedTransactionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the expenses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Auth::user()->transactions()->expenses()->get();

        return view('expenses.index', compact('transactions'));
    }


    /**
     * Displays the create an expense page
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::getUserCategoriesByType(config('constants.TYPE_INDEX.expense'));

        $fixed_transactions = FixedTransaction::getUserFixedTransactionsByType(config('constants.TYPE_INDEX.expense'));

        $accounts = Auth::user()->accounts()->lists('title','id');

        session()->flash('back_url', Request::server('HTTP_REFERER'));

        return view('expenses.create', compact('fixed_transactions', 'categories', 'accounts'));
    }

    /**
     * Creates a new expense
     *
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TransactionRequest $request)
    {
//        $stored = Auth::user()->transactions()->create($request->all());
        $transaction              = new Transaction();
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->user_id     = Auth::user()->id;
        $transaction->category_id = $request->input('category_id');
        $transaction->account_id  = $request->input('account_id');
        $transaction->type_id     = config('constants.TYPE_INDEX.expense');;
        $transaction->paid_date   = $request->input('paid_date');
        $transaction->payment_date_limit   = $request->input('payment_date_limit');
        // This conditional is for displaying the transactions in the correct month of payment. If it has been paid it is displayed with the creation date
        $transaction->published_date = empty($request->input('paid_date')) ? Date::now() :$request->input('paid_date');

        DB::beginTransaction();

        $stored = $transaction->save();

        //if the transaction has an associated account then save the statistic
        if (!empty($transaction->account_id))
        {
            $stored_statistic1 = AccountStatistic::addAccountStatistic($transaction, 'expense');
            $stored_statistic2 = GeneralStatistic::addGeneralStatistic($transaction, 'expense');

            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error creando la estadística del gasto.');
                session()->flash('flash_action', 'danger');

                return Redirect::to(session()->get('back_url'));         
            }            
        }  

        if ($stored)
        {
            DB::commit();

            session()->flash('flash_message', 'Gasto creado.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error creando el gasto.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::to(session()->get('back_url'));
    }

    /**
     * View the edit an expense page
     *
     * @param Transaction $transaction
     * @return \Illuminate\View\View
     */
    public function edit(Transaction $transaction)
    {
        $categories = Category::getUserCategoriesByType(config('constants.TYPE_INDEX.expense'));

        $fixed_transactions = FixedTransaction::getUserFixedTransactionsByType(config('constants.TYPE_INDEX.expense'));

        $accounts = Auth::user()->accounts()->lists('title','id');

        session()->flash('back_url', Request::server('HTTP_REFERER'));

        return view('expenses.edit', compact('transaction', 'fixed_transactions', 'categories', 'accounts'));
    }

    /**
     * Updates an expense
     *
     * @param Transaction $transaction
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Transaction $transaction, TransactionRequest $request)
    {
        //value used for statistics
        $transaction_old = Auth::user()->transactions->find($transaction->id);

//        $updated = $transaction->update($request->all());
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->category_id = $request->input('category_id');
        $transaction->account_id  = $request->input('account_id');
        $transaction->paid_date   = $request->input('paid_date');
        $transaction->payment_date_limit   = $request->input('payment_date_limit');

        // This conditional is for displaying the transactions in the correct month of payment. If it has not been paid it is displayed with the creation date
        $transaction->published_date = empty($request->input('paid_date')) ? $transaction->created_at : $request->input('paid_date');
        
        DB::beginTransaction();

        $updated = $transaction->update();        

        //if the transaction has an associated account then update the statistic
        if (!empty($transaction->account_id) && !empty($transaction_old->account_id))
        {
            $stored_statistic1 = AccountStatistic::updateAccountStatistic($transaction, $transaction_old, 'expense');
            $stored_statistic2 = GeneralStatistic::updateGeneralStatistic($transaction, $transaction_old, 'expense');

            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error actualizando la estadística del gasto.');
                session()->flash('flash_action', 'danger');

                return Redirect::back();
            }            
        }
        //if the transaction didn' had an account associated (ex: created with a fixed transaction)
        else if (!empty($transaction->account_id) && empty($transaction_old->account_id))
        {
            $stored_statistic1 = AccountStatistic::addAccountStatistic($transaction, 'expense');
            $stored_statistic2 = GeneralStatistic::addGeneralStatistic($transaction, 'expense');

            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error creando la estadística del gasto.');
                session()->flash('flash_action', 'danger');

                Redirect::to(session()->get('back_url'));
            }
        }

        if ($updated)
        {
            DB::commit();

            session()->flash('flash_message', 'Gasto actualizado.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error actualizando el gasto.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::to(session()->get('back_url'));
    }

    /**
     * Deletes an expense
     *
     * @param Transaction $transaction
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Transaction $transaction)
    {
        //value used for statistics
        $transaction_old = clone $transaction;

        DB::beginTransaction();

        $deleted = $transaction->delete();

        //if the transaction has an associated account then remove the statistic
        if (!empty($transaction->account_id))
        {
            $stored_statistic1 = AccountStatistic::removeTransactionAccountStatistic($transaction_old, 'expense');
            $stored_statistic2 = GeneralStatistic::removeGeneralStatistic($transaction_old, 'expense');


            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error borrando la estadística del gasto.');
                session()->flash('flash_action', 'danger');

                return Redirect::back();       
            }  
        }

        if ($deleted)
        {
            DB::commit();
            
            session()->flash('flash_message', 'Gasto borrado.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error actualizando el gasto.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::back();
    }


    /**
     * Get transaction id and title for use in jQuery autocomplete select field
     *
     * @param Request $request
     * @return array
     */
    public function getTransactionByTitle(Request $request)
    {
        $title = $request->input('term');
    
        $results = array();

        $data = Transaction::where('title', 'like', "%".$title."%")->get();

        foreach ($data as $row)
        {
            $results[] = [ 'id' => $row->id, 'value' => $row->title ];
        }

        return $results;
    }
}
