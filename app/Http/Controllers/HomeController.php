<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\AccountStatistic;
use App\Transaction;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        if (Auth::user())
        {
            if (!is_null($request->get('search')) && !empty(trim($request->get('search'))))
            {
                $search = $request->get('search');
                // Get the transactions that have been paid grouped by month and year
                $transactions = Auth::user()->transactions()->order()->search($search)->get();

                $balance = Transaction::calculateIncomesExpenses($transactions);

                $transactions = Transaction::groupByMonth($transactions);

                return view('search', compact('user', 'transactions', 'balance'));
            }
            else
            {
                // Used to check if user has categories
                $categories = Auth::user()->categories()->get();

                // Used to check if user has accounts and to get all the account's total amount
                $accounts = Auth::user()->accounts()->orderBy('type_id')->get();

                // Get the transactions that have been paid grouped by month and year
                $transactions = Auth::user()->transactions()->order()->get();
                $transactions = Transaction::groupByMonth($transactions);

                // Get the global total debit amount
                $total_debit = AccountStatistic::calculateTotalAmountByAccountType(config('constants.ACCOUNT_TYPE_INDEX.debit')) +
                    AccountStatistic::calculateTotalAmountByAccountType(config('constants.ACCOUNT_TYPE_INDEX.cash'));

                $total_debit_formatted = number_format($total_debit, 0, '.', '.');

                // Get the global total credit amount
                $total_credit = AccountStatistic::calculateTotalAmountByAccountType(config('constants.ACCOUNT_TYPE_INDEX.credit')) +
                    AccountStatistic::calculateTotalAmountByAccountType(config('constants.ACCOUNT_TYPE_INDEX.debt'));

                $total_credit_formatted = number_format($total_credit, 0, '.', '.');

                return view('home', compact('user', 'categories', 'accounts', 'transactions', 'total_debit_formatted', 'total_credit_formatted'));
            }
        }
        else
        {
            return view('home', compact('user'));
        }
    }

}
