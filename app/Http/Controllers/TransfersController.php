<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\TransferRequest;
use App\Http\Requests;
use App\Transaction;
use App\Category;
use App\AccountStatistic;

class TransfersController extends Controller
{

    /**
     * Loads Authentication middleware for all methods
     *
     * FixedTransactionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the expenses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Auth::user()->transactions()->transfers()->get();

        return view('transfers.index', compact('transactions'));
    }


    /**
     * Displays the create an expense page
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $accounts = Auth::user()->accounts()->lists('title','id');

        $categories = Category::getUserCategoriesByType(0);

        session()->flash('back_url', Request::server('HTTP_REFERER'));

        return view('transfers.create', compact('categories', 'accounts'));
    }

    /**
     * Creates a new transfer
     *
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TransferRequest $request)
    {
//        $stored = Auth::user()->transactions()->create($request->all());
        $transaction              = new Transaction();
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->user_id     = Auth::user()->id;
        $transaction->category_id = $request->input('category_id');
        $transaction->account_id  = $request->input('account_id');
        $transaction->account2_id = $request->input('account2_id');
        $transaction->type_id     = config('constants.TYPE_INDEX.transfer');
        $transaction->paid_date   = $request->input('paid_date');

        // This conditional is for displaying the transactions in the correct month of payment. If it has been paid it is displayed with the creation date
        $transaction->published_date = empty($request->input('paid_date')) ? date(Y-m-d) : $request->input('paid_date');
        
        DB::beginTransaction();

        $stored = $transaction->save();

        //if the transaction has an associated account then save the statistic
        if (!empty($transaction->account_id) && !empty($transaction->account2_id))
        {
            $stored_statistics = AccountStatistic::calculateAccountStatisticsTransferOperation($transaction);

            if (!$stored_statistics)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error creando la estadística de la transferencia.');
                session()->flash('flash_action', 'danger');

                return Redirect::to(session()->get('back_url'));     
            }               
        }

        if ($stored)
        {
            DB::commit();

            session()->flash('flash_message', 'Transferencia creada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error creando la transferencia.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::to(session()->get('back_url'));
    }

    /**
     * View the edit a transfer page
     *
     * @param Transaction $transaction
     * @return \Illuminate\View\View
     */
    public function edit(Transaction $transaction)
    {
        $accounts = Auth::user()->accounts()->lists('title','id');

        $categories = Category::getUserCategoriesByType(0);

        session()->flash('back_url', Request::server('HTTP_REFERER'));

        return view('transfers.edit', compact('transaction', 'categories', 'accounts'));
    }

    /**
     * Updates a transfer
     *
     * @param Transaction $transaction
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Transaction $transaction, TransferRequest $request)
    {
        //value used for statistics
        $transaction_old = Auth::user()->transactions->find($transaction->id);

//        $updated = $transaction->update($request->all());
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->category_id = $request->input('category_id');
        $transaction->account_id  = $request->input('account_id');
        $transaction->account2_id  = $request->input('account2_id');
        $transaction->paid_date   = $request->input('paid_date');

        // This conditional is for displaying the transactions in the correct month of payment. If it has not been paid it is displayed with the creation date
        $transaction->published_date = empty($request->input('paid_date')) ? $transaction->created_at : $request->input('paid_date');
        
        DB::beginTransaction();

        $updated = $transaction->update();        

        //if the transaction has an associated account then update the statistic
        if ($transaction->account_id != null && $transaction->account2_id != null)
        {
            // if the user didn't update the accounts
            if ($transaction_old->account_id == $transaction->account_id && $transaction_old->account2_id == $transaction->account2_id)
            {
                $stored_statistics = AccountStatistic::calculateAccountStatisticsTransferOperation($transaction, 'update', $transaction_old);
            }
            else
            {
                // check if the transfer had an associated account
                if ($transaction_old->account_id)
                {
                    $stored_statistics1 = AccountStatistic::calculateAccountStatisticsTransferOperation($transaction_old, 'destroy', $transaction_old);
                    $stored_statistics2 = AccountStatistic::calculateAccountStatisticsTransferOperation($transaction, 'store');

                    $stored_statistics = $stored_statistics1 && $stored_statistics2;
                }
                // if updating the transfer and it didn't had any account associated (if created from a fixed transaction)
                else
                {
                    $stored_statistics = AccountStatistic::calculateAccountStatisticsTransferOperation($transaction);
                }
            }

            if (!$stored_statistics)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error actualizando la estadística de la transferencia.');
                session()->flash('flash_action', 'danger');

                return Redirect::to(session()->get('back_url'));       
            }              
        }

        if ($updated)
        {
            DB::commit();

            session()->flash('flash_message', 'Transferencia actualizada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error actualizando la transferencia.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::to(session()->get('back_url'));
    }

    /**
     * Deletes an expense
     *
     * @param Transaction $transaction
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Transaction $transaction)
    {
        //value used for statistics
        $transaction_old = Auth::user()->transactions->find($transaction->id);
                
        DB::beginTransaction();                

        $deleted = $transaction->delete();

        //if the transaction has an associated account then destroy the statistic
        if ($transaction_old->account_id != null && $transaction_old->account2_id != null)
        {
            $stored_statistics = AccountStatistic::calculateAccountStatisticsTransferOperation($transaction, 'destroy', $transaction_old);

            if (!$stored_statistics)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error borrando la estadística de la transferencia.');
                session()->flash('flash_action', 'danger');

                return Redirect::to(session()->get('back_url'));       
            }              
        }

        if ($deleted)
        {
            DB::commit();

            session()->flash('flash_message', 'Transferencia borrada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error borrando la transferencia.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::back();
    }
}
