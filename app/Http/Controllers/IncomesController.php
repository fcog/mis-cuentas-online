<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use App\Http\Requests\TransactionRequest;
use App\Http\Requests;
use App\FixedTransaction;
use App\Transaction;
use App\Category;
use App\AccountStatistic;
use App\GeneralStatistic;


class IncomesController extends Controller
{

    /**
     * Loads Authentication middleware for all methods
     *
     * FixedTransactionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getTransactions']);
    }

    /**
     * Display a listing of the transactions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Auth::user()->transactions()->incomes()->get();

        return view('incomes.index', compact('transactions'));
    }


    /**
     * Displays the create a transaction page
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::getUserCategoriesByType(config('constants.TYPE_INDEX.income'));

        $fixed_transactions = FixedTransaction::getUserFixedTransactionsByType(config('constants.TYPE_INDEX.income'));

        $accounts = Auth::user()->accounts()->lists('title','id');

        session()->flash('back_url', Request::server('HTTP_REFERER'));

        return view('incomes.create', compact('categories', 'fixed_transactions', 'accounts'));
    }

    /**
     * Creates a new transaction
     *
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws Exception
     */
    public function store(TransactionRequest $request)
    {
//        $created = Auth::user()->transactions()->create($request->all());
        $transaction              = new Transaction();
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->user_id     = Auth::user()->id;
        $transaction->category_id = $request->input('category_id');
        $transaction->account_id  = $request->input('account_id');
        $transaction->type_id     = config('constants.TYPE_INDEX.income');
        $transaction->paid_date   = $request->input('paid_date');
        $transaction->payment_date_limit   = $request->input('payment_date_limit');
        // This conditional is for displaying the transactions in the correct month of payment. If it has been paid it is displayed with the creation date
        $transaction->published_date = empty($request->input('paid_date')) ? Date::now() : $request->input('paid_date');

        DB::beginTransaction();

        $stored = $transaction->save();

        //if the transaction has an associated account then save the statistic
        if (!empty($transaction->account_id))
        {
            $stored_statistic1 = AccountStatistic::addAccountStatistic($transaction, 'income');
            $stored_statistic2 = GeneralStatistic::addGeneralStatistic($transaction, 'income');

            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error creando la estadística del ingreso.');
                session()->flash('flash_action', 'danger');

                Redirect::to(session()->get('back_url'));
            }
        }

        if ($stored)
        {
            DB::commit();

            session()->flash('flash_message', 'El ingreso ha sido creado.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error creando la estadística del ingreso.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::to(session()->get('back_url'));
    }

    /**
     * View the create the edit a transaction
     *
     * @param Transaction $transaction
     * @return \Illuminate\View\View
     */
    public function edit(Transaction $transaction)
    {
        $categories = Category::getUserCategoriesByType(config('constants.TYPE_INDEX.income'));

        $fixed_transactions = FixedTransaction::getUserFixedTransactionsByType(config('constants.TYPE_INDEX.income'));

        $accounts = Auth::user()->accounts()->lists('title','id');

        session()->flash('back_url', Request::server('HTTP_REFERER'));

        return view('incomes.edit', compact('transaction', 'fixed_transactions', 'categories', 'accounts'));
    }

    /**
     * Updates a transaction
     *
     * @param Transaction $transaction
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Transaction $transaction, TransactionRequest $request)
    {
        //value used for statistics
        $transaction_old = Auth::user()->transactions->find($transaction->id);

        //$updated = $transaction->update($request->all());
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->category_id = $request->input('category_id');
        $transaction->account_id  = $request->input('account_id');
        $transaction->paid_date   = $request->input('paid_date');
        $transaction->payment_date_limit   = $request->input('payment_date_limit');
        // This conditional is for displaying the transactions in the correct month of payment. If it has been paid it is displayed with the creation date
        $transaction->published_date = empty($request->input('paid_date')) ? $transaction->created_at : $request->input('paid_date');
        
        DB::beginTransaction();

        $updated = $transaction->update();

        //if the transaction has an associated account then update the statistic
        if (!empty($transaction->account_id) && !empty($transaction_old->account_id))
        {
            $stored_statistic1 = AccountStatistic::updateAccountStatistic($transaction, $transaction_old, 'income');
            $stored_statistic2 = GeneralStatistic::updateGeneralStatistic($transaction, $transaction_old, 'income');

            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error actualizando la estadística del ingreso.');
                session()->flash('flash_action', 'danger');

                return Redirect::back();
            }            
        }
        //if the transaction didn' had an account associated (ex: created with a fixed transaction)
        else if (!empty($transaction->account_id) && empty($transaction_old->account_id))
        {
            $stored_statistic1 = AccountStatistic::addAccountStatistic($transaction, 'income');
            $stored_statistic2 = GeneralStatistic::addGeneralStatistic($transaction, 'income');

            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error creando la estadística del ingreso.');
                session()->flash('flash_action', 'danger');

                Redirect::to(session()->get('back_url'));
            }
        }

        if ($updated)
        {
            DB::commit();

            session()->flash('flash_message', 'El ingreso ha sido actualizado.');
            session()->flash('flash_action', 'success'); 
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error actualizando la estadística del ingreso.');
            session()->flash('flash_action', 'danger');
        }      

        return Redirect::to(session()->get('back_url'));
    }

    /**
     * Deletes a transaction
     *
     * @param Transaction $transaction
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Transaction $transaction)
    {
        //value used for statistics
        $transaction_old = clone $transaction;

        DB::beginTransaction();

        $deleted = $transaction->delete();

        //if the transaction has an associated account then remove the statistic
        if (!empty($transaction->account_id))
        {
            $stored_statistic1 = AccountStatistic::removeTransactionAccountStatistic($transaction_old, 'income');
            $stored_statistic2 = GeneralStatistic::removeGeneralStatistic($transaction_old, 'income');


            if (!$stored_statistic1 || !$stored_statistic2)
            {
                DB::rollBack();

                session()->flash('flash_message', 'Hubo un error borrando la estadística del ingreso.');
                session()->flash('flash_action', 'danger');

                return Redirect::back();       
            }
        }  

        if ($deleted)
        {
            DB::commit();
            
            session()->flash('flash_message', 'El ingreso ha sido borrado.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            DB::rollBack();

            session()->flash('flash_message', 'Hubo un error borrando el ingreso.');
            session()->flash('flash_action', 'danger');
        }

        return Redirect::back();
    }


    //for jQuery autocomplete
    public function getTransactionByTitle(Request $request)
    {
        $title = $request->input('term');
    
        $results = array();

        $data = Transaction::where('title', 'like', "%".$title."%")->get();

        foreach ($data as $row)
        {
            $results[] = [ 'id' => $row->id, 'value' => $row->title ];
        }

        return $results;
    }

    public function getTransactions()
    {
        if (Auth::user())
        {
            return Auth::user()->transactions->get();
        }

        return 0;
    } 
}
