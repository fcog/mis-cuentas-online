<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Requests;
use App\Http\Requests\FixedTransactionRequest;
use App\FixedTransaction;
use Illuminate\Support\Facades\Auth;
use App\Category;

class FixedTransactionsController extends Controller
{
	private $types;

	/**
	 * Loads Authentication middleware for all methods
	 *
	 * FixedTransactionController constructor.
	 */
	public function __construct()
	{
		$this->middleware('auth');

		$this->types = Collection::make(['Seleccione...', config('constants.TYPE.expense'), config('constants.TYPE.income'), config('constants.TYPE.transfer')]);
	}

	/**
	 * View the active fixed transactions
	 *
	 * @return \Illuminate\View\View
     */
	public function index()
	{
		$fixed_transactions = Auth::user()->fixedTransactions;

		return view('fixed_transactions.index', compact('fixed_transactions'));
	}

	/**
	 * View the inactive fixed transactions
	 *
	 * @return \Illuminate\View\View
	 */
	public function indexInactive()
	{
		$fixed_transactions = Auth::user()->fixedTransactions()->onlyTrashed()->get();

		return view('fixed_transactions.index_inactive', compact('fixed_transactions'));
	}

	/**
	 * View a fixed transaction
	 *
	 * @param FixedTransaction $fixed_transaction
	 * @return \Illuminate\View\View
     */
	public function show(FixedTransaction $fixed_transaction)
	{
		return view('fixed_transactions.show', compact('fixed_transaction'));
	}

	/**
	 * View the create a fixed transaction page
	 *
	 * type: 1.fixed expense 2.fixed income 3.debt
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		$types = $this->types;

		$categories = Category::getUserCategoriesByType(0);

		return view('fixed_transactions.create', compact('types', 'categories'));
	}

	/**
	 * View the edit a fixed transaction page
	 *
	 * @param FixedTransaction $fixed_transaction
	 * @return \Illuminate\View\View
	 */
	public function edit(FixedTransaction $fixed_transaction)
	{
		$types = $this->types;

		$categories = Category::getUserCategoriesByType(0);

		return view('fixed_transactions.edit', compact('fixed_transaction', 'types', 'categories'));
	}

	/**
	 * Creates a fixed transaction
	 *
	 * @param FixedTransactionRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store(FixedTransactionRequest $request)
	{
		$created = Auth::user()->fixedTransactions()->create($request->all());

		if ($created)
		{
			session()->flash('flash_message', 'La transacción fija ha sido creada.');
			session()->flash('flash_action', 'success');
		}
		else
		{
			session()->flash('flash_message', 'Hubo un error creando la transacción fija');
			session()->flash('flash_action', 'danger');
		}

		return redirect()->route('fixed-transactions.index');
	}

	/**
	 * Activates a fixed transaction
	 *
	 * @param FixedTransaction $fixed_transaction
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function softStore(FixedTransaction $fixed_transaction)
	{
		$activated = $fixed_transaction->restore();

		if ($activated)
		{
			session()->flash('flash_message', 'La transacción fija ha sido activada.');
			session()->flash('flash_action', 'success');
		}
		else
		{
			session()->flash('flash_message', 'Hubo un error activando la transacción fija');
			session()->flash('flash_action', 'danger');
		}

		return redirect()->route('fixed-transactions.indexInactive');
	}

	/**
	 * Updates a fixed transaction
	 *
	 * @param FixedTransaction $fixed_transaction
	 * @param FixedTransactionRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update(FixedTransaction $fixed_transaction, FixedTransactionRequest $request)
	{
		$updated = $fixed_transaction->update($request->all());

		if ($updated)
		{
			session()->flash('flash_message', 'La transacción fija ha sido actualizada.');
			session()->flash('flash_action', 'success');
		}
		else
		{
			session()->flash('flash_message', 'Hubo un error actualizando la transacción fija');
			session()->flash('flash_action', 'danger');
		}

		return redirect()->route('fixed-transactions.index');
	}

	/**
	 * Inactivates a fixed transaction
	 *
	 * @param FixedTransaction $fixed_transaction
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function softDelete(FixedTransaction $fixed_transaction)
	{
		$deactivated = $fixed_transaction->delete();

		if ($deactivated)
		{
			session()->flash('flash_message', 'La transacción fija ha sido desactivada.');
			session()->flash('flash_action', 'success');
		}
		else
		{
			session()->flash('flash_message', 'Hubo un error desactivando la transacción fija');
			session()->flash('flash_action', 'danger');
		}

		return redirect()->route('fixed-transactions.index');
	}

	/**
	 * Deletes a category
	 *
	 * @param FixedTransaction $fixed_transaction
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function destroy(FixedTransaction $fixed_transaction)
	{
		$deleted = $fixed_transaction->forceDelete();

		if ($deleted)
		{
			session()->flash('flash_message', 'La transacción fija ha sido borrada.');
			session()->flash('flash_action', 'success');
		}
		else
		{
			session()->flash('flash_message', 'Hubo un error borrando la transacción fija');
			session()->flash('flash_action', 'danger');
		}

		return redirect()->route('fixed-transactions.index');
	}

	/**
	 * Get a fixed transaction by title. JSON for jQuery auto completion
	 *
	 * @param Request $request
	 * @return array
     */
	public function getFixedTransactionsByTitle(Request $request)
	{
		$title = $request->input('term');

		return FixedTransaction::getFixedTransactionsByTitle($title);
	}

	/**
	 * converts the fixed transactions into transactions each month
	 *
	 * @param $month
	 * @return bool
	 */
	public function generateMonthlyFixedTransactions(Request $request)
	{
		$month = $request->input('month');

		$ok = FixedTransaction::generateMonthlyFixedTransactions($month);

		if ($ok)
		{
			session()->flash('flash_message', 'Las transacción fijas se sincronizaron correctamente.');
			session()->flash('flash_action', 'success');
		}
		else
		{
			session()->flash('flash_message', 'Hubo un error sincronizando las transacción fijas');
			session()->flash('flash_action', 'danger');
		}

		return redirect()->route('fixed-transactions.index');
	}

	/**
	 * converts the fixed transactions into transactions each month, url for cron only
	 *
	 * @return bool
	 */
	public function generateMonthlyFixedTransactionsCron()
	{
		return FixedTransaction::generateMonthlyFixedTransactions();
	}
}