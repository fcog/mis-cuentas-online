<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Requests;
use App\Http\Requests\AccountRequest;
use App\Account;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Transaction;
use App\AccountStatistic;

class AccountsController extends Controller
{
    private $types;

    /**
     * Loads Authentication middleware for all methods
     *
     * FixedTransactionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->types = Collection::make([
            'Seleccione...',
            config('constants.ACCOUNT_TYPE.debit'),
            config('constants.ACCOUNT_TYPE.cash'),
            config('constants.ACCOUNT_TYPE.credit'),
            config('constants.ACCOUNT_TYPE.debt')
        ]);
    }

    /**
     * View the active accounts
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $accounts = Auth::user()->accounts;

        return view('accounts.index', compact('accounts'));
    }

    /**
     * View the inactive accounts
     *
     * @return \Illuminate\View\View
     */
    public function indexInactive()
    {
        $accounts = Auth::user()->accounts()->onlyTrashed()->get();

        return view('accounts.index_inactive', compact('accounts'));
    }

    /**
     * View an account
     *
     * @param Account $account
     * @return \Illuminate\View\View
     */
    public function show(Account $account)
    {
        return view('accounts.show', compact('account'));
    }

    /**
     * View the create an account page
     *
     * type: 1.fixed expense 2.fixed income 3.debt
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $types = $this->types;

        $categories = Category::getUserCategoriesByType(0);

        return view('accounts.create', compact('types', 'categories'));
    }

    /**
     * View the edit an account page
     *
     * @param Account $account
     * @return \Illuminate\View\View
     */
    public function edit(Account $account)
    {
        $types = $this->types;

        $categories = Category::getUserCategoriesByType(0);

        return view('accounts.edit', compact('account', 'types', 'categories'));
    }

    /**
     * Creates an account
     *
     * @param AccountRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(AccountRequest $request)
    {
        $created = Auth::user()->accounts()->create($request->all());

        if ($created)
        {
            //create empty statistic
            $transaction = new Transaction();
            $transaction->account_id = $created->id;
            $transaction->published_date = date('Y-m-d');

            AccountStatistic::saveNewAccountStatistic($transaction);

            session()->flash('flash_message', 'La cuenta ha sido creada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error creando la cuenta');
            session()->flash('flash_action', 'danger');
        }

        return redirect()->route('accounts.index');
    }

    /**
     * Activates an account
     *
     * @param Account $account
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function softStore(Account $account)
    {
        $activated = $account->restore();

        if ($activated)
        {
            session()->flash('flash_message', 'La cuenta ha sido activada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error activando la cuenta');
            session()->flash('flash_action', 'danger');
        }

        return redirect()->route('accounts.indexInactive');
    }

    /**
     * Updates an account
     *
     * @param Account $account
     * @param AccountRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Account $account, AccountRequest $request)
    {
        $updated = $account->update($request->all());

        if ($updated)
        {
            session()->flash('flash_message', 'La cuenta ha sido actualizada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error actualizando la cuenta');
            session()->flash('flash_action', 'danger');
        }

        return redirect()->route('accounts.index');
    }

    /**
     * Inactivates an account
     *
     * @param Account $account
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function softDelete(Account $account)
    {
        $deactivated = $account->delete();

        if ($deactivated)
        {
            session()->flash('flash_message', 'La cuenta ha sido desactivada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error desactivando la cuenta');
            session()->flash('flash_action', 'danger');
        }

        return redirect()->route('accounts.index');
    }

    /**
     * Deletes an account
     *
     * @param Account $account
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Account $account)
    {
        $deleted = $account->forceDelete();

        if (is_null($deleted))
        {
            session()->flash('flash_message', 'La cuenta ha sido borrada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error borrando la cuenta');
            session()->flash('flash_action', 'danger');
        }

        return redirect()->route('accounts.index');
    }

    /**
     * Get an account by title. JSON for jQuery auto completion
     *
     * @param Request $request
     * @return array
     */
    public function getAccountsByTitle(Request $request)
    {
        $title = $request->input('term');

        return Account::getAccountsByTitle($title);
    }
}