<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Transaction;
use App\AccountStatistic;
use App\GeneralStatistic;
use Khill\Lavacharts\Formats\DateFormat;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

class StatisticsController extends Controller
{

    /**
     * Loads Authentication middleware for all methods
     *
     * FixedTransactionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Columns chart-----------------------------------------------------------------------------

        // Get the transactions that have been paid grouped by month and year
        $statistics = Auth::user()->generalStatistics->groupBy(
            function($item) {
                $statistic_month = Date::createFromFormat('Y-m-d H:i:s', $item->month);
                return $statistic_month->format('Y-m');
            });

        $data = \Lava::DataTable();

        $data->addDateColumn('Year')
                 ->setDateTimeFormat('Y-m')
                 ->addNumberColumn(config('constants.TYPE.income'))
                 ->addNumberColumn(config('constants.TYPE.expense'));

        foreach ($statistics as $date => $statistics_month)
        {
            $carbonDate = Date::createFromFormat('Y-m', $date);

            $data->addRow( array($carbonDate->format('Y-m'), $statistics_month->first()->total_amount_incomes, $statistics_month->first()->total_amount_expenses) );
            //$data->addRow(array('2009-1-1', 1100, 490));
        }

        \Lava::ColumnChart('Balance')
                   ->setOptions(array(
                     'datatable' => $data,
                     'title' => 'Balance de mis cuentas',
                     'titleTextStyle' => \Lava::TextStyle(array(
                         'color' => '#317eac',
                       'fontSize' => 16
                     )),
                       'hAxis' =>  \Lava::HorizontalAxis(array(
                           'format' => 'MMM y'
                       ))
                   ));

        // Pie chart-------------------------------------------------------------------------------------

        $statistics_total = GeneralStatistic::getCurrentGeneralStatisticByDate(Date::now());

        $statistics_total_expenses = $statistics_total->total_amount_expenses;

        $paid_expenses = Auth::user()->transactions()->paidExpenses()->whereYear('published_date', '=', date('Y'))->whereMonth('published_date', '=', date('m'))->get();

        $paid_transfer_expenses = Transaction::getActualTransferExpenses();

        $all_expenses = $paid_transfer_expenses->merge($paid_expenses);

        $statistics = $all_expenses->groupBy(
            function($item) {
                return $item->category->title;
            });

        $data = \Lava::DataTable();

        $data->addStringColumn('Categorias')
                ->addNumberColumn('Porcentaje');

        foreach ($statistics as $key => $statistic) 
        {
            $data->addRow(array($key, $statistic->sum('amount_paid')));
            //$data->addRow(array('Settle Argument', 89));
        }

        \Lava::PieChart('Categorias')
             ->setOptions(array(
                'datatable' => $data,
                'title' => 'Categoria de gastos',
                'titleTextStyle' => \Lava::TextStyle(array(
                     'color' => '#317eac',
                   'fontSize' => 16
                 )),               
               'is3D' => true,
                 'slices' => array(
                    \Lava::Slice(array(
                      'offset' => 0.2
                    )),
                    \Lava::Slice(array(
                      'offset' => 0.25
                    )),
                    \Lava::Slice(array(
                      'offset' => 0.3
                    ))
                  )
              ));

        return view('statistics.index');
    }
}
