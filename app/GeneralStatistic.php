<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GeneralStatistic extends Model
{
    protected $dates = ['month'];

    /**
     * Get the user associated with the AccountStatistic
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Returns the general statistic of the given transaction's date
     * @param $transaction
     * @return mixed
     */
    private static function getCurrentGeneralStatistic($transaction)
    {
        return Auth::user()->generalStatistics()->whereYear('month', '=', $transaction->published_date->year)->whereMonth('month', '=', $transaction->published_date->format('m'))->first();
    }

    /**
     * Returns the general statistic of the given date
     * @param $transaction
     * @return mixed
     */
    public static function getCurrentGeneralStatisticByDate($date)
    {
        return Auth::user()->generalStatistics()->whereYear('month', '=', $date->year)->whereMonth('month', '=', $date->format('m'))->first();
    }

    /**
     * Saves the general statitic of the given transaction
     *
     * @param $transaction
     * @param $operation
     * @return bool|int
     */
    public static function addGeneralStatistic($transaction, $operation)
    {
        $statistic = GeneralStatistic::getCurrentGeneralStatistic($transaction);

        if ($statistic == null)
        {
            $statistic = new GeneralStatistic();
            $statistic->month = $transaction->published_date;
            $statistic->user_id = Auth::user()->id;
        }

        switch ($operation)
        {
            case 'income':
                $statistic->total_amount_incomes += $transaction->amount_paid;
                $statistic->total_transactions_incomes += 1;
                break;

            case 'expense':
                $statistic->total_amount_expenses += $transaction->amount_paid;
                $statistic->total_transactions_expenses += 1;
        }

        return $statistic->save();
    }

    /**
     * Updates the general statistic of the given transation
     *
     * @param $transaction
     * @param $transaction_old
     * @param $operation
     * @return bool|int
     */
    public static function updateGeneralStatistic($transaction, $transaction_old, $operation)
    {
        $statistic = GeneralStatistic::getCurrentGeneralStatistic($transaction);

        switch ($operation)
        {
            case 'income':
                $statistic->total_amount_incomes -= $transaction_old->amount_paid;
                $statistic->total_amount_incomes += $transaction->amount_paid;
                break;

            case 'expense':
                $statistic->total_amount_expenses -= $transaction_old->amount_paid;
                $statistic->total_amount_expenses += $transaction->amount_paid;
        }

        return $statistic->save();
    }

    /**
     * Removes the general statistic of the given transation
     *
     * @param $transaction
     * @param $operation
     * @return bool|int
     */
    public static function removeGeneralStatistic($transaction, $operation)
    {
        $statistic = GeneralStatistic::getCurrentGeneralStatistic($transaction);

        switch ($operation)
        {
            case 'income':
                $statistic->total_amount_incomes -= $transaction->amount_paid;
                $statistic->total_transactions_incomes -= 1;
                break;

            case 'expense':
                $statistic->total_amount_expenses -= $transaction->amount_paid;
                $statistic->total_transactions_expenses -= 1;
        }

        return $statistic->save();
    }
}
