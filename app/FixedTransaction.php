<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laracasts\Presenter\PresentableTrait;

class FixedTransaction extends Model
{
    use SoftDeletes;
    use PresentableTrait;

    protected $dates = ['paid_date'];

    protected $presenter = 'App\Presenters\FixedTransactionPresenter';

    protected $fillable = [
    	'title',
    	'type_id',
    	'amount',
    	'category_id',
    	'user_id',
    	'paid_date',
    	'payment_day',
    ];

    /**
     * Returns the type name of the category defined in constants file
     *
     * @return string
     */
    public function getType()
    {
        switch ($this->type_id)
        {
            case config('constants.TYPE_INDEX.expense'):
                $type = config('constants.TYPE.expense');
                break;
            case config('constants.TYPE_INDEX.income'):
                $type = config('constants.TYPE.income');
                break;
            case config('constants.TYPE_INDEX.transfer'):
                $type = config('constants.TYPE.transfer');
                break;
            default:
                $type = null;
                break;
        }

        return $type;
    }

    /**
     * Mutator for cleaning the amount to only numbers, because the form sends the field with the point character
     *
     * @param $amount_paid
     */
    public function setAmountAttribute($amount)
    {
        if (!empty($amount))
        {
            $this->attributes['amount'] = preg_replace('/[^0-9]+/', '', $amount);
        }
    }    
    
    /**
     * A Fixed Transaction belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * A Fixed Transaction belongs to a category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the logged user's fixed transaction by title.
     *
     * @param String $title
     * @return array
     */
    public static function getFixedTransactionsByTitle($title)
    {
        return Auth::user()->fixedTransactions()->where('title', 'like', "%".$title."%")->lists('title', 'id');
    }

    /**
     * Get the logged user's fixed transactions by type
     *
     * @param $type_id
     * @return mixed
     */
    public static function getUserFixedTransactionsByType($type_id)
    {
        return Auth::user()->fixedTransactions()->where('type_id', $type_id)->lists('title', 'id');
    }

    /**
     * converts the fixed transactions into transactions in the given month
     *
     * @param $month
     * @return bool
     */
    public static function generateMonthlyFixedTransactions($month)
    {
        //Get all the user's fixed transactions
        $fixed_transactions = Auth::user()->fixedTransactions;

        //QA variable
        $ok = true;

        foreach ($fixed_transactions as $fixed_transaction)
        {
            //Check if the fixed transactions hasn't been created in the present month
            $transaction_old = Auth::user()->transactions()->where('fixed_transaction_id', $fixed_transaction->id)->whereMonth('published_date', '=', $month)->first();

            if ($transaction_old === null)
            {
                //Create transaction from the fixed transaction
                $transaction_new = new Transaction;
                $transaction_new->type_id = $fixed_transaction->type_id;
                $transaction_new->title = $fixed_transaction->title;
                $transaction_new->fixed_transaction_id = $fixed_transaction->id;
                $transaction_new->amount_paid = $fixed_transaction->amount;
                $transaction_new->category_id = $fixed_transaction->category_id;
                $payment_date = date('Y') . "-" . $month . "-" . $fixed_transaction->payment_day;
                $transaction_new->payment_date_limit = $payment_date;
                $published_date = date('Y') . "-" . $month . "-01";
                $transaction_new->published_date = $published_date;
                $transaction_new->user_id = Auth::user()->id;

                $stored = $transaction_new->save();

                if (!$stored)
                {
                    $ok = false;
                }
            }
        }

        return $ok;
    }
}