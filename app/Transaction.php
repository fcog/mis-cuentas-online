<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class Transaction extends Model
{
    use PresentableTrait;

    protected $dates = ['paid_date', 'payment_date_limit', 'published_date'];

    protected $presenter = 'App\Presenters\TransactionPresenter';

    protected $fillable = [
        'title',
    	'fixed_transactions_id',
    	'amount_paid',
        'user_id',
    	'category_id',
        'account_id',
        'type_id',
    	'paid_date',
    	'payment_date_limit',
    ];

    
    /**
     * Mutator (this is runned before saving the property in the database) for saving the Published Date with Hour and Minutes
     *
     * @param $date
     */
    public function setPublishedDateAttribute($date)
    {
        if (!empty($date) && !is_object($date))
        {
            $this->attributes['published_date'] = Carbon::createFromFormat('Y-m-d', $date);
        }
    }
    
    /**
     * Mutator (this is runned before saving the property in the database) for saving the Paid Date with Hour and Minutes
     *
     * @param $date
     */
    public function setPaidDateAttribute($date)
    {
        if (!empty($date) && !is_object($date))
        {
            $this->attributes['paid_date'] = Carbon::createFromFormat('Y-m-d', $date);
        }
        else if (empty($date))
        {
            $this->attributes['paid_date'] = null;
        }
    }

    /**
     * Mutator for saving the Payment Date Limit with Hour and Minutes
     *
     * @param $date
     */
    public function setPaymentDateLimitAttribute($date)
    {
        if (!empty($date))
        {
            $this->attributes['payment_date_limit'] = Carbon::createFromFormat('Y-m-d', $date);
        }
    }

    /**
     * Mutator for cleaning the amount to only numbers, because the form sends the field with the point character
     *
     * @param $amount_paid
     */
    public function setAmountPaidAttribute($amount_paid)
    {
        if (!empty($amount_paid))
        {
            $this->attributes['amount_paid'] = preg_replace('/[^0-9]+/', '', $amount_paid);
        }
    }


    /**
     * Returns the type name of the category defined in constants file
     *
     * @return string
     */
    public function getType()
    {
        switch ($this->type_id)
        {
            case config('constants.TYPE_INDEX.expense'):
                $type = config('constants.TYPE.expense');
                break;
            case config('constants.TYPE_INDEX.income'):
                $type = config('constants.TYPE.income');
                break;
            case config('constants.TYPE_INDEX.transfer'):
                $type = config('constants.TYPE.transfer');
                break;
            default:
                $type = null;
                break;
        }

        return $type;
    }

    /**
     * Returns the type name of the account defined in constants file
     *
     * @return string
     */
    public function getAccount()
    {
        if ($this->account === null)
        {
            return null;
        }

        switch ($this->account->type_id)
        {
            case 1:
                $account = config('constants.ACCOUNT_TYPE.debit') . " - " . $this->account->title;
                break;
            case 2:
                $account = config('constants.ACCOUNT_TYPE.cash') . " - " . $this->account->title;
                break;
            case 3:
                $account = config('constants.ACCOUNT_TYPE.credit') . " - " . $this->account->title;
                break;
            case 4:
                $account = config('constants.ACCOUNT_TYPE.debt') . " - " . $this->account->title;
                break;
            default:
                $account = null;
                break;
        }

        return $account;
    }

    /**
     * Returns the type name of the account defined in constants file
     * Account2 is used to stored the account destination when making a transfer
     *
     * @return string
     */
    public function getAccount2()
    {
        if ($this->account2 === null)
        {
            return null;
        }

        switch ($this->account2->type_id)
        {
            case 1:
                $account = config('constants.ACCOUNT_TYPE.debit') . " - " . $this->account2->title;
                break;
            case 2:
                $account = config('constants.ACCOUNT_TYPE.cash') . " - " . $this->account2->title;
                break;
            case 3:
                $account = config('constants.ACCOUNT_TYPE.credit') . " - " . $this->account2->title;
                break;
            case 4:
                $account = config('constants.ACCOUNT_TYPE.debt') . " - " . $this->account2->title;
                break;
            default:
                $account = null;
                break;
        }

        return $account;
    }

    /**
     * Scope function.
     *
     * Get the expense transactions
     *
     * @param $query
     */
    public function scopeExpenses($query)
    {
     	$query->where('type_id', config('constants.TYPE_INDEX.expense'));
    }

    /**
     * Scope function.
     *
     * Get the paid expense transactions
     *
     * @param $query
     */
    public function scopePaidExpenses($query)
    {
        $query->where('type_id', config('constants.TYPE_INDEX.expense'))->whereNotNull('account_id');
    }

    /**
     * Scope function.
     *
     * Get the incomes transactions
     *
     * @param $query
     */
    public function scopeIncomes($query)
    {
        $query->where('type_id', config('constants.TYPE_INDEX.income'));
    }

    /**
     * Scope function.
     *
     * Get the paid incomes transactions
     *
     * @param $query
     */
    public function scopePaidIncomes($query)
    {
        $query->where('type_id', config('constants.TYPE_INDEX.income'))->whereNotNull('account_id');
    }    

    /**
     * Scope function.
     *
     * Get the transfer transactions
     *
     * @param $query
     */
    public function scopeTransfers($query)
    {
        $query->where('type_id', config('constants.TYPE_INDEX.transfer'));
    }

    /**
     * Scope function.
     *
     * Get the paid transfer transactions
     *
     * @param $query
     */
    public function scopePaidTransfers($query)
    {
        $query->where('type_id', config('constants.TYPE_INDEX.transfer'))->whereNotNull('account_id')->whereNotNull('account2_id');
    }

    public function scopeSearch($query, $search)
    {
        if (!empty($search))
        {
            $query->where('title', 'LIKE', '%' . $search . '%');
        }
    }

    /**
     * returns a collection of transactions grouped by month, year
     *
     * @param $query
     */
    public static function scopeOrder($query)
    {
        $query->orderBy('published_date', 'DESC')->orderBy('payment_date_limit');
    }

    /**
     * Get the user associated with the Transaction
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the account associated with the Transaction
     */
    public function account()
    {
        return $this->belongsTo('App\Account')->withTrashed();
    }

    /**
     * Get the account destination associated with the Transaction of type transfer
     */
    public function account2()
    {
        return $this->belongsTo('App\Account', 'account2_id')->withTrashed();
    }

    /**
     * Get the category record associated with the Transaction
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * returns a collection of transactions grouped by month, year
     *
     * @param $transactions
     * @return
     * @internal param $query
     */
    public static function groupByMonth($transactions)
    {
        return $transactions->groupBy(
            function($item) {
                $published_date = Date::createFromFormat('Y-m-d H:i:s', $item->published_date);
                return $published_date->format('F, Y');
            });
    }

    /**
     * Returns the general statistic of the given transaction's date
     * @return mixed
     */
    public function getGeneralStatistic()
    {
        return Auth::user()->generalStatistics()->whereYear('month', '=', $this->published_date->year)->whereMonth('month', '=', $this->published_date->format('m'))->first();
    }

    public static function getActualTransferExpenses()
    {
        $transfer_expenses = new Collection;

        $actualTransfers = Auth::user()->transactions()->paidTransfers()->whereYear('published_date', '=', date('Y'))->whereMonth('published_date', '=', date('m'))->get();

        foreach ($actualTransfers as $transfer) 
        {
            switch ($transfer->account->type_id) 
            {
                case config('constants.ACCOUNT_TYPE_INDEX.debit'):
                case config('constants.ACCOUNT_TYPE_INDEX.cash'):

                    if ($transfer->account2->type_id == config('constants.ACCOUNT_TYPE_INDEX.credit') || 
                        $transfer->account2->type_id == config('constants.ACCOUNT_TYPE_INDEX.debt')) 
                    {
                        $transfer_expenses->push($transfer);
                    }
                    break;               

                case config('constants.ACCOUNT_TYPE_INDEX.credit'):
                case config('constants.ACCOUNT_TYPE_INDEX.debt'):

                    if ($transfer->account2->type_id == config('constants.ACCOUNT_TYPE_INDEX.debit') || 
                        $transfer->account2->type_id == config('constants.ACCOUNT_TYPE_INDEX.cash') ||
                        $transfer->account2->type_id == config('constants.ACCOUNT_TYPE_INDEX.credit') || 
                        $transfer->account2->type_id == config('constants.ACCOUNT_TYPE_INDEX.debt') )
                    {
                        $transfer_expenses->push($transfer);
                    }
                    break;
            }
        }

        return $transfer_expenses;
    }

    public static function calculateIncomesExpenses($transactions)
    {
        $result = array('incomes' => 0, 'expenses' => 0);

        foreach ($transactions as $transaction) 
        {
            switch ($transaction->getType()) 
            {
                case config('constants.TYPE.income'):

                    $result['incomes'] += $transaction->amount_paid;

                case config('constants.TYPE.expense'):

                    $result['expenses'] += $transaction->amount_paid;
            }
        }

        return $result;
    }
}