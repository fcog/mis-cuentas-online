<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class FixedTransactionPresenter extends Presenter
{

    /**
     * Format the transaction amount paid in money format
     *
     * @return string
     */
    public function amount_formatted()
    {
        return "$" . number_format($this->amount, 0, '.', '.');
    }

}