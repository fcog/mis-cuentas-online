<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;
use Jenssegers\Date\Date;

class TransactionPresenter extends Presenter
{

    /**
     * Format the transaction amount paid in money format
     *
     * @return string
     */
    public function amount_paid_formatted()
    {
        return @$this->amount_paid ? "$" . number_format($this->amount_paid, 0, '.', '.') : null;
    }

    /**
     * format the paid date in "time ago" and translated using Jenssegers\Date package
     *
     * @return string
     */
    public function paid_date_formatted()
    {
        return @$this->paid_date ? Date::createFromFormat('Y-m-d H:i:s', $this->paid_date)->format('l, d F') : null;
    }

    /**
     * Check if the transaction has payment date set and then formats it for the index view
     *
     * @return null
     */
    public function payment_date_limit_formatted()
    {
        return @$this->payment_date_limit ? Date::createFromFormat('Y-m-d H:i:s', $this->payment_date_limit)->format('l, d F') : null;
    }
}