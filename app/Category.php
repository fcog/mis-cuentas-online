<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Category extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'title',
    	'type_id',
    	'user_id',
    ];

    /**
     * Returns the type name of the category defined in constants file
     *
     * @return string
     */
    public function getType()
	{
        if ($this->type_id == 1)
        {
            return config('constants.TYPE.expense');
        }
        else if ($this->type_id == 2)
        {
            return config('constants.TYPE.income');
        }
        else return null;
    }

    // public function scopeActive($query){
    // 	$query->where('active', '=', TRUE);
    // }

    // public function scopeInactive($query){
    // 	$query->where('active', '=', FALSE);
    // }

	/**
	 * Get the logged user's categories by type
	 *
	 * @param $type_id
	 * @return mixed
     */
	public static function getUserCategoriesByType($type_id){

		if ($type_id == 0 || $type_id == 3)
		{
			return Auth::user()->categories()->lists('title', 'id');
		}
		else
		{
			return Auth::user()->categories()->where('type_id', $type_id)->lists('title', 'id');
		}
	}

	/**
	 * A Category belongs to a user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
