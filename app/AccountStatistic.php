<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class AccountStatistic extends Model
{
    protected $dates = ['month'];

    /**
     * Get the user associated with the AccountStatistic
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the account associated with the AccountStatistic
     */
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    /**
     * Returns the new statistic
     *
     * @param $transaction
     * @return AccountStatistic
     */
    public static function saveNewAccountStatistic($transaction)
    {
        $statistic             = new AccountStatistic();
        $statistic->account_id = $transaction->account_id;
        $statistic->user_id    = Auth::user()->id;
        $statistic->month      = $transaction->published_date;
        $statistic->total_amount = 0;

        $statistic->save();

        return $statistic;
    }

    /**
     * add account statistic for an income and expense transaction
     *
     * @param $transaction
     * @param $type
     * @return bool|int
     */
    public static function addAccountStatistic($transaction, $type)
    {
        $statistic = AccountStatistic::getCurrentAccountStatistic($transaction);

        switch ($transaction->account->type_id)
        {
            case config('constants.ACCOUNT_TYPE_INDEX.debit'):
            case config('constants.ACCOUNT_TYPE_INDEX.cash'):

                switch ($type)
                {
                    case 'income':
                        $statistic->total_amount += $transaction->amount_paid;
                        break;
                    case 'expense':
                        $statistic->total_amount -= $transaction->amount_paid;
                }

                break;

            case config('constants.ACCOUNT_TYPE_INDEX.credit'):
            case config('constants.ACCOUNT_TYPE_INDEX.debt'):

                switch ($type)
                {
                    case 'income':
                        $statistic->total_amount -= $transaction->amount_paid;
                        break;
                    case 'expense':
                        $statistic->total_amount += $transaction->amount_paid;
                }

                break;
        }

        $statistic->total_transactions += 1;

    	return $statistic->update();
    }

    /**
     * update account statistics for an income or expense transaction
     *
     * @param $transaction
     * @param $transaction_old
     * @param $type
     * @return bool
     */
    public static function updateAccountStatistic($transaction, $transaction_old, $type)
    {
        $statistic_old = AccountStatistic::getCurrentAccountStatistic($transaction_old);

        switch ($statistic_old->account->type_id)
        {
            case config('constants.ACCOUNT_TYPE_INDEX.debit'):
            case config('constants.ACCOUNT_TYPE_INDEX.cash'):

                switch ($type)
                {
                    case 'income':
                        $statistic_old->total_amount -= $transaction_old->amount_paid;
                        break;

                    case 'expense':
                        $statistic_old->total_amount += $transaction_old->amount_paid;
                        break;
                }

                break;

            case config('constants.ACCOUNT_TYPE_INDEX.credit'):
            case config('constants.ACCOUNT_TYPE_INDEX.debt'):

                switch ($type)
                {
                    case 'income':
                        $statistic_old->total_amount += $transaction_old->amount_paid;
                        break;

                    case 'expense':
                        $statistic_old->total_amount -= $transaction_old->amount_paid;
                        break;
                }

                break;
        }

        $success1 = $statistic_old->update();

        $statistic = AccountStatistic::getCurrentAccountStatistic($transaction);

        switch ($statistic->account->type_id)
        {
            case config('constants.ACCOUNT_TYPE_INDEX.debit'):
            case config('constants.ACCOUNT_TYPE_INDEX.cash'):

                switch ($type)
                {
                    case 'income':
                        $statistic->total_amount += $transaction->amount_paid;
                        break;

                    case 'expense':
                        $statistic->total_amount -= $transaction->amount_paid;
                        break;
                }

                break;

            case config('constants.ACCOUNT_TYPE_INDEX.credit'):
            case config('constants.ACCOUNT_TYPE_INDEX.debt'):

                switch ($type)
                {
                    case 'income':
                        $statistic->total_amount -= $transaction->amount_paid;
                        break;

                    case 'expense':
                        $statistic->total_amount += $transaction->amount_paid;
                        break;
                }

                break;
        }

        $success2 = $statistic->update();

        return $success1 && $success2;
    }

    /**
     * Removes transaction amount from statistic 
     * 
     * @param $transaction
     * @param $type
     */
    public static function removeTransactionAccountStatistic($transaction, $type)
    {
        $statistic = AccountStatistic::getCurrentAccountStatistic($transaction);

        switch ($type) 
        {
            case 'income':
                $statistic->total_amount -= $transaction->amount_paid;
                break;

            case 'expense':
                $statistic->total_amount += $transaction->amount_paid;
                break;
        }

        $statistic->total_transactions -= 1;

        return $statistic->update();
    }    

    /**
     * Get the last statistic of the given account
     * 
     * @param $transaction
     * @return collection
     */
    public static function getLastAccountStatistic($transaction)
    {
    	return Auth::user()->statistics()->where('account_id', $transaction->account_id)->whereYear('month', '<=', $transaction->published_date->year)->whereMonth('month', '<=', $transaction->published_date->format('m'))->orderBy('month', 'desc')->first();
    }

    /**
     * Get the current statistic of the given transaction
     * 
     * @param $transaction
     * @return collection
     */
    public static function getCurrentAccountStatistic($transaction)
    {
    	$statistic = Auth::user()->statistics()->where('account_id', $transaction->account_id)->whereYear('month', '=', $transaction->published_date->year)->whereMonth('month', '=', $transaction->published_date->format('m'))->first();

        if ($statistic == null)
        {
            $statistic = AccountStatistic::saveNewAccountStatistic($transaction);
        }

        return $statistic;
    }

    /**
     * Get the current statistic of the given transaction (uses account2_id (for transfers))
     * 
     * @param $transaction
     * @return collection
     */
    public static function getCurrentAccountStatistic2($transaction)
    {
        $statistic = Auth::user()->statistics()->where('account_id', $transaction->account2_id)->whereYear('month', '=', $transaction->published_date->year)->whereMonth('month', '=', $transaction->published_date->format('m'))->first();

        if ($statistic == null)
        {
            $statistic = AccountStatistic::saveNewAccountStatistic($transaction);
        }

        return $statistic;
    }    

    /**
     * Get the actual statistic of the given account
     * 
     * @param $account_id
     * @return collection
     */
    public static function getActualAccountAccountStatistic($account_id)
    {
        return Auth::user()->statistics()->where('account_id', $account_id)->whereYear('month', '=', date('Y'))->whereMonth('month', '=', date('m'))->first();
    }

    /**
     * update account statistic of a transaction of type transfer
     *
     * @param $statistic
     * @param $transaction_amount
     * @param $operation
     * @param $action
     */
    public static function updateTransferAccountStatistic($statistic, $transfer, $operation, $action = null)
    {
        switch ($operation)
        {
            case 'sum':
                $statistic->total_amount += $transfer->amount_paid;
                break;

            case 'discount':
                $statistic->total_amount -= $transfer->amount_paid;
                break;
        }

        switch ($action)
        {
            case 'store':
                $statistic->total_transactions += 1;
                break;

            case 'destroy':
                $statistic->total_transactions -= 1;
                break;
        }

        return $statistic->update();
    }

    /**
     * Calculates the statistics income and expense amount of a transfer's accounts+
     * Used in the AccountStatistics Controller
     *
     * amount_old is used when the transfer is been updated
     *
     * @param $transfer
     * @param string $action
     * @param int $transfer_old
     * @return bool
     */
    public static function calculateAccountStatisticsTransferOperation($transfer, $action = 'store', $transfer_old = null)
    {
        $account1_type_id = $transfer->account->type_id;
        $account2_type_id = $transfer->account2->type_id;

        $statistic1 = AccountStatistic::getCurrentAccountStatistic($transfer);
        $statistic2 = AccountStatistic::getCurrentAccountStatistic2($transfer);

        switch ($account1_type_id) 
        {
            case config('constants.ACCOUNT_TYPE_INDEX.debit'):
            case config('constants.ACCOUNT_TYPE_INDEX.cash'):

                switch ($account2_type_id) 
                {
                	// debit to debit; debit to cash; cash to debit; cash to cash
                    case config('constants.ACCOUNT_TYPE_INDEX.debit'):
                    case config('constants.ACCOUNT_TYPE_INDEX.cash'):
                        
                        // discount on account 1 debit/cash
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'sum', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'discount', $action);
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'discount', $action);
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'sum', $action);
                        }

                        // sum on account 2 debit/cash
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer_old, 'discount', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'sum', $action);
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'sum', $action);
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer_old, 'discount', $action);
                        }

                        break;
                    
                    // debit to credit; debit to debt; cash to credit; cash to debt
                    case config('constants.ACCOUNT_TYPE_INDEX.credit'):
                    case config('constants.ACCOUNT_TYPE_INDEX.debt'):

                    	// discount on account 1 debit/cash
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'sum', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'discount', $action);
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'discount', $action);
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'sum', $action);
                        }

                        // discount on account 2 credit/debt
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer_old, 'sum', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'discount', $action);
                            // updated an expense (payed a credit)
                            GeneralStatistic::updateGeneralStatistic($transfer, $transfer_old, 'expense');
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'discount', $action);
                            // made an expense (payed a credit)
                            GeneralStatistic::addGeneralStatistic($transfer, 'expense');
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer_old, 'sum', $action);
                            // removed the expense (removed the credit payment)
                            GeneralStatistic::removeGeneralStatistic($transfer, 'expense');
                        }

                        break;
                }
                break;

            case config('constants.ACCOUNT_TYPE_INDEX.credit'):
            case config('constants.ACCOUNT_TYPE_INDEX.debt'):

                switch ($account2_type_id) 
                {
                	// credit to debit; credit to cash; debt to debit; debt to cash
                    case config('constants.ACCOUNT_TYPE_INDEX.debit'):
                    case config('constants.ACCOUNT_TYPE_INDEX.cash'):
                        
                        // sum on account 1 credit/debt
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'discount', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'sum', $action);
                            // updated an expense (income from credit)
                            GeneralStatistic::updateGeneralStatistic($transfer, $transfer_old, 'expense');
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'sum', $action);
                            // made an expense (income from credit)
                            GeneralStatistic::addGeneralStatistic($transfer, 'expense');
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'discount', $action);
                            // removed an expense (income from credit)
                            GeneralStatistic::removeGeneralStatistic($transfer_old, 'expense');
                        }

                        // sum on account 2 debit/cash
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer_old, 'discount', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'sum', $action);
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'sum', $action);
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'discount', $action);
                        }

                        break;
                    
                    // credit to credit; credit to debt; credit to credit; credit to debt
                    case config('constants.ACCOUNT_TYPE_INDEX.credit'):
                    case config('constants.ACCOUNT_TYPE_INDEX.debt'):

                    	// sum on account 1 credit/debt
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'discount', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'sum', $action);
                            // updated an expense (income from credit)
                            GeneralStatistic::updateGeneralStatistic($transfer, $transfer_old, 'expense');
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer, 'sum', $action);
                            // made an expense (income from credit)
                            GeneralStatistic::addGeneralStatistic($transfer, 'expense');
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic1, $transfer_old, 'discount', $action);
                            // removed an expense (income from credit)
                            GeneralStatistic::removeGeneralStatistic($transfer_old, 'expense');
                        }

                        // discount on account 2 credit/debt
                        if ($action == 'update')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer_old, 'sum', $action);
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'discount', $action);
                        }
                        elseif ($action == 'store')
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer, 'discount', $action);
                        }
                        else
                        {
                            AccountStatistic::updateTransferAccountStatistic($statistic2, $transfer_old, 'sum', $action);
                        }

                        break;
                }
                break;
        }

        return true;
    }

    /**
     * Calculates all the total money amount of all the accounts of a given type
     * If the type is not given then it calculates all accounts
     *
     * @return int
     */
    public static function calculateTotalAmountByAccountType($type_id = 0, $date = null)
    {
    	$statistics = array();

        switch ($type_id)
        {
            case config('constants.ACCOUNT_TYPE_INDEX.debit'):
            case config('constants.ACCOUNT_TYPE_INDEX.cash'):
            case config('constants.ACCOUNT_TYPE_INDEX.credit'):
            case config('constants.ACCOUNT_TYPE_INDEX.debt'):

            	$accounts = Auth::user()->accounts()->where('type_id', $type_id)->first();

            	//if the user has accounts
            	if ($accounts)
            	{
	            	if ($date == null)
	            	{
	            		$statistics = $accounts->statistics()->where('month', '<=', date('Y-m'))->get();
	            	}
	            	else
	            	{
	            		$statistics = $accounts->statistics()->where('month', '<=', $date)->get();
	            	}
                }

                break;
            default:
                $statistics = Auth::user()->statistics()->where('month', '<=', date('Y-m'))->get();
        }

        $total = 0;

        foreach ($statistics as $statistic)
        {
            $total += $statistic->total_amount;
        }

        return $total;
    }
  
}
