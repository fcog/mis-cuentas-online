<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),            
            'title' => 'Salario',
            //type: 1.expense 2.income
            'type_id' => 2,
            'user_id' => 1
        ]);
	    DB::table('categories')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),            
            'title' => 'Freelance',
            //type: 1.expense 2.income
            'type_id' => 2,
            'user_id' => 1
        ]);        
        DB::table('categories')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),            
            'title' => 'Salud',
            //type: 1.expense 2.income
            'type_id' => 1,
            'user_id' => 1
        ]);
        DB::table('categories')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),            
            'title' => 'Servicios',
            //type: 1.expense 2.income
            'type_id' => 1,
            'user_id' => 1
        ]);           
        DB::table('categories')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),            
            'title' => 'Deuda',
            //type: 1.expense 2.income
            'type_id' => 1,
            'user_id' => 1
        ]);
        DB::table('categories')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),            
            'title' => 'Regalo',
            //type: 1.expense 2.income
            'type_id' => 1,
            'user_id' => 1
        ]); 
    }
}
