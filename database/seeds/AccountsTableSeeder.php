<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'title' => 'Davivienda',
            //type: 1.debit 2.cash 3.credit 4.debt
            'type_id' => 1,
            'user_id' => 1
        ]);
	    DB::table('accounts')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),	    	
            'title' => 'Billetera',
            //type: 1.debit 2.cash 3.credit 4.debt
            'type_id' => 2,
            'user_id' => 1
        ]);        
        DB::table('accounts')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'title' => 'Citibank',
            //type: 1.debit 2.cash 3.credit 4.debt
            'type_id' => 3,
            'user_id' => 1
        ]);
        DB::table('accounts')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'title' => 'Banco de Bogotá',
            //type: 1.debit 2.cash 3.credit 4.debt
            'type_id' => 3,
            'user_id' => 1
        ]);
    }
}