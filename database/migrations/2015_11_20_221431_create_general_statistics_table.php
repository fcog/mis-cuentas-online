<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('month');
            $table->integer('user_id')->unsigned();
            $table->integer('total_amount_incomes')->default(0);
            $table->integer('total_amount_expenses')->default(0);
            $table->integer('total_transactions_incomes')->default(0);
            $table->integer('total_transactions_expenses')->default(0);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_statistics');
    }
}
