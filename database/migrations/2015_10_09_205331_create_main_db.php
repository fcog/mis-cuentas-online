<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            //type: 1.fixed expense 2.fixed income
            $table->integer('type_id');
            $table->string('title')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('category_id')->default(0);
            $table->integer('payment_day');
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });

        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            //type: 1.expense 2.income
            $table->integer('type_id')->unsigned();
            $table->integer('account_id')->nullable()->unsigned();
            $table->integer('fixed_transaction_id')->nullable()->unsigned();
            $table->string('title')->nullable();
            $table->integer('amount_paid')->nullable()->unsigned();
            $table->integer('category_id')->default(0);
            $table->integer('payment_date_limit')->nullable();
            $table->timestamp('paid_date')->nullable();
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });  

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            //type: 1.expense 2.income
            $table->integer('type_id')->unsigned();
            $table->string('title');
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            //type: 1.debit 2.credit 3.cash 4.debt
            $table->integer('type_id')->unsigned();
            $table->string('title');
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fixed_transactions');
        Schema::drop('transactions');
        Schema::drop('categories');
        Schema::drop('accounts');
    }
}
