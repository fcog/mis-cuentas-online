<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('account_statistics', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->timestamp('month');
                $table->integer('user_id')->unsigned();
                $table->integer('account_id');
                $table->integer('total_amount');
                $table->integer('total_transactions')->default(0);

                $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade');

                $table->foreign('account_id')
                      ->references('id')
                      ->on('accounts')
                      ->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_statistics');
    }
}
