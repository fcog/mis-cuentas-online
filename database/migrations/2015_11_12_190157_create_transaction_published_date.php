<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionPublishedDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        ALTER TABLE {tableName} ADD COLUMN COLNew {type};
        Schema::table('transactions', function($table)
        {
            $table->timestamp('published_date')->default('2015-11-11 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function($table)
        {        
            $table->dropColumn(array('published_date'));
        });
    }
}
