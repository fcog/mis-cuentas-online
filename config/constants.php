<?php

return [
    'TYPE' => [
        'expense' => 'Gasto',
        'income' => 'Ingreso',
        'transfer' => 'Transferencia'
    ],
    'TYPE_INDEX' => [
        'expense' => 1,
        'income' => 2,
        'transfer' => 3
    ],
    'ACCOUNT_TYPE' => [
        'debit' => 'Cuenta de ahorro',
        'cash' => 'Efectivo',        
        'credit' => 'Cuenta de crédito',
        'debt' => 'Deuda',
    ],
    'ACCOUNT_TYPE_INDEX' => [
        'debit' => 1,
        'cash' => 2,
        'credit' => 3,
        'debt' => 4,
    ],
];