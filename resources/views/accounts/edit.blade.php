@extends('layout')


@section('content')

    <div class="page-header">
        <h1>Editar una cuenta</h1>
    </div>

    @include('errors._form_errors')

    <div class="well">

        {!! Form::model($account, ['method' => 'PATCH', 'action' => ['AccountsController@update', $account]]) !!}

            @include('accounts._form', ['submitButtonText' => 'Editar'])

        {!! Form::close() !!}

    </div>

@stop
