@extends('layout')

@section('content')

	<h1>Cuenta #{{ $account->id }}</h1>

	<p>Tipo: {{ $account->type_id }}</p>
	<p>Nombre: {{ $account->title }}</p>
	<p>Monto: {{ $account->amount }}</p>

@stop

@section('footer')

@stop