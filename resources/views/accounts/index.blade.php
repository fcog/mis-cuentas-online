@extends('layout')

@section('content')

	<div class="page-header">
		<h1>Cuentas</h1>
	</div>

	<div class="panel panel-primary">
		<!-- Default panel contents -->
		<div class="panel-heading">Acciones</div>
		<div class="panel-body">
			<a class="btn btn-success" href="{{ route('accounts.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear nueva</a>
			<a class="btn btn-default" href="{{ route('accounts.indexInactive') }}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver inactivas</a>
		</div>
	</div>

	@if (count($accounts))
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
			        <tr>
			          	<th>Nombre</th>
						<th>Tipo</th>
			        	<th>Acción</th>
			        </tr>
		        </thead>
		        <tbody>
				@foreach ($accounts as $account)

					<tr>
						<td>{{ $account->title }}</td>
						<td>{{ $account->getType() }}</td>
						<td>
							<a alt="Editar" title="Editar" href="{{ route('accounts.edit', $account) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
							<a alt="Desactivar" title="Desactivar" href="{{ route('accounts.deactivate', $account) }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						</td>
					</tr>

				@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="alert alert-warning" role="alert">No hay datos</div>
	@endif

@stop

@section('footer')

@stop