<div class="form-group">

    {!! Form::label('title', 'Título:') !!}
    {!! Form::text('title', null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('type_id', 'Tipo:') !!}
    {!! Form::select('type_id', $types, null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-success']) !!}

</div>