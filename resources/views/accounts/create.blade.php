@extends('layout')


@section('content')

	<div class="page-header">
		<h1>Crear una cuenta</h1>
	</div>

	@include('errors._form_errors')

	<div class="well">

		{!! Form::open(['url' => 'accounts']) !!}

			@include('accounts._form', ['submitButtonText' => 'Crear'])

		{!! Form::close() !!}

	</div>

@stop