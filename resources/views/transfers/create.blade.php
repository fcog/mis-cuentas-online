@extends('layout')


@section('content')

	<div class="page-header">
		<h1>Crear Transferencia</h1>
	</div>

    @include('errors._form_errors')

	<div class="well">

		{!! Form::open(['url' => 'transfers']) !!}

			@include('transfers._form', ['submitButtonText' => 'Crear'])

		{!! Form::close() !!}

	</div>

@stop
