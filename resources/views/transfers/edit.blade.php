@extends('layout')


@section('content')

    <div class="page-header">
        <h1>Editar Transferencia</h1>
    </div>

    @include('errors._form_errors')

    <div class="well">

        {!! Form::model($transaction, ['method' => 'PATCH', 'action' => ['TransfersController@update', $transaction]]) !!}

            @include('transfers._form', ['submitButtonText' => 'Guardar'])

        {!! Form::close() !!}

    </div>

@stop
