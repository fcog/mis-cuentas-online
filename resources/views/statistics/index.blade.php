@extends('layout')

@section('content')

	<div class="page-header">
		<h1>Estadisticas</h1>
	</div>

	<div class="panel panel-primary">
		<!-- Default panel contents -->
		<div class="panel-heading">Acciones</div>
		<div class="panel-body">
			<a class="btn btn-default" href="{{ route('expenses.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear nuevo</a>
		</div>
	</div>

	<div id="balance-div"></div>

	<div id="categorias-div"></div>

	@columnchart('Balance', 'balance-div')

	@piechart('Categorias', 'categorias-div')

@stop

@section('footer')

@stop