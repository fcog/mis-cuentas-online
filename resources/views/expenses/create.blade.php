@extends('layout')


@section('content')

	<div class="page-header">
		<h1>Crear gasto</h1>
	</div>

    @include('errors._form_errors')

	<div class="well">

		{!! Form::open(['url' => 'expenses']) !!}

			@include('expenses._form', ['submitButtonText' => 'Crear'])

		{!! Form::close() !!}

    </div>

@stop
