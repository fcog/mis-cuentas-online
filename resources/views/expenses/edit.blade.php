@extends('layout')


@section('content')

    <div class="page-header">
        <h1>Editar gasto</h1>
    </div>

    @include('errors._form_errors')

    <div class="well">

        {!! Form::model($transaction, ['method' => 'PATCH', 'action' => ['ExpensesController@update', $transaction]]) !!}

            @include('expenses._form', ['submitButtonText' => 'Guardar'])

        {!! Form::close() !!}

    </div>

@stop
