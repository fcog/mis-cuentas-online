<div class="form-group">

    {!! Form::label('category_id', 'Categoria:') !!}
    {!! Form::select('category_id', array(0 => 'Seleccione...') + $categories->toArray(), null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('title', 'Título:') !!}
    {!! Form::text('title', null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('amount_paid', 'Monto:') !!}
    <div class="input-group">
        <span class="input-group-addon">$</span>
        {!! Form::input('text','amount_paid', null, ['class'=>'form-control']) !!}
    </div>

</div>

<div class="form-group">

    {!! Form::label('account_id', 'Cuenta:') !!}
    {!! Form::select('account_id',  array(0 => 'Seleccione...') + $accounts->toArray(), null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('paid_date', 'Fecha de pago:') !!}
    {!! Form::text('paid_date', @$transaction->paid_date ? $transaction->paid_date->format('Y-m-d') : null, ['class'=>'form-control', 'autocomplete' => 'off']) !!}

</div>

<div class="form-group">

    {!! Form::label('payment_date_limit', 'Fecha límite de pago:') !!}
    {!! Form::text('payment_date_limit', @$transaction->payment_date_limit ? $transaction->payment_date_limit->format('Y-m-d') : null, ['class'=>'form-control', 'autocomplete' => 'off']) !!}

</div>

<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-success']) !!}

</div>

@section('javascript')
    <script>
        $( document ).ready(function() {

            //format amount paid input field with a point each 3 numbers
            $("#amount_paid").keyup(function(event) {

                // skip for arrow keys
                if(event.which >= 37 && event.which <= 40) return;

                // format number
                $(this).val(function(index, value) {
                    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                });
            });

            $( "#paid_date" )
                    .datepicker()
                    .datepicker( "option", "dateFormat", 'yy-m-d' )
                    @if ($submitButtonText == "Crear")
                    .datepicker( 'setDate', new Date() )
                    @elseif ($transaction->paid_date)
                    .datepicker( 'setDate', '{{ $transaction->paid_date->format('Y-m-d') }}' )
                    @endif
                    .keyup(function(e) {
                        if(e.keyCode == 8 || e.keyCode == 46) {
                            $(this).val('');
                            $(this).removeAttr('value');
                        }
                    });

            $( "#payment_date_limit" )
                    .datepicker()
                    .datepicker( "option", "dateFormat", 'yy-m-d' )
                    @if ($submitButtonText == "Guardar" && $transaction->payment_date_limit)
                    .datepicker( 'setDate', '{{ $transaction->payment_date_limit->format('Y-m-d') }}' )
                    @endif
                    .keyup(function(e) {
                        if(e.keyCode == 8 || e.keyCode == 46) {
                            $(this).val('');
                            $(this).removeAttr('value');
                        }
                    });

        });
    </script>
@stop
