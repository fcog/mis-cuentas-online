<!DOCTYPE html>
<html>
<head>
	<title>Mis Cuentas Online</title>

    <meta name=viewport content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/all.css') }}" >

	@yield('header')

</head>
<body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Mis Cuentas Online</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
              <li>
                  <a href="{{ route('expenses.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                      Gastos
                      <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                      <li><a href="{{ route('expenses.index') }}">Ver Gastos</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('expenses.create') }}">Crear Gasto</a></li>
                  </ul>
              </li>
              <li>
                  <a href="{{ route('incomes.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                      Ingresos
                      <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                      <li><a href="{{ route('incomes.index') }}">Ver Ingresos</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('incomes.create') }}">Crear Ingreso</a></li>
                  </ul>
              </li>
              <li>
                  <a href="{{ route('transfers.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                      Transferencias
                      <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                      <li><a href="{{ route('transfers.index') }}">Ver Transferencias</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('transfers.create') }}">Crear Transferencia</a></li>
                  </ul>
              </li>
              <li>
                  <a href="{{ route('fixed-transactions.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                      Transacciones Fijas
                      <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                      <li><a href="{{ route('fixed-transactions.index') }}">Ver Transacciónes Fijas</a></li>
                      <li><a href="{{ route('fixed-transactions.indexInactive') }}">Ver Transacciónes Fijas Inactivas</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('fixed-transactions.create') }}">Crear Transacción Fija</a></li>
                  </ul>
              </li>
              <li>
                  <a href="{{ route('accounts.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                      Cuentas
                      <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                      <li><a href="{{ route('accounts.index') }}">Ver Cuentas</a></li>
                      <li><a href="{{ route('accounts.indexInactive') }}">Ver Cuentas Inactivas</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('accounts.create') }}">Crear Cuenta</a></li>
                  </ul>
              </li>
              <li>
                  <a href="{{ route('categories.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                      Categorias
                      <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                      <li><a href="{{ route('categories.index') }}">Ver Categorias</a></li>
                      <li><a href="{{ route('categories.indexInactive') }}">Ver Categorias Inactivas</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('categories.create') }}">Crear Categoria</a></li>
                  </ul>
              </li>
              <li><a href="/statistics">Estadísticas</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li class="active">
                  @if($user)
                      <a href="#" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                          <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                          {{ $user->name }}
                          <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu">
                          <li><a href="#">Mi Cuenta</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="/auth/logout"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Salir</a></li>
                      </ul>
                  @else
                     <a href="/auth/login"><span class="glyphicon glyphicon-user" aria-hidden="true"></span><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Ingresar</a>
                  @endif
              </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

	<div class="container">
        @if (Session::has('flash_message'))
            <div class="alert alert-{{ Session::get('flash_action') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_message') }}
            </div>
        @endif

		@yield('content')
	</div>

	@yield('footer')

    <footer class="footer">
        <div class="container">
            <p class="text-muted">2016 - Aplicación web creada por <a href="http://franciscogiraldo.com" target="_blank">Francisco Giraldo</a> </p>
        </div>
    </footer>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        //enable nav dropdown menu on hover
        $(function() {
            $('.nav li').hover(function(){
                $(this).addClass('open');
            }, function(){
                $(this).removeClass('open');
            });
        });

    </script>

    @yield('javascript')

</body>
</html>