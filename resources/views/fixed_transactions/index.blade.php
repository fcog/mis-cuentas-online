@extends('layout')

@section('content')

	<div class="page-header">
		<h1>Transacciones Fijas</h1>
	</div>

	<div class="panel panel-primary">
		<!-- Default panel contents -->
		<div class="panel-heading">Acciones</div>
		<div class="panel-body">
			<a class="btn btn-success" href="{{ route('fixed-transactions.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear nueva</a>
			<a class="btn btn-default" href="{{ route('fixed-transactions.indexInactive') }}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver inactivas</a>
                        <a class="btn btn-default" href="{{ route('fixed-transactions.sync', ['month' => date('m')]) }}"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Sincronizar</a>
		</div>
	</div>

	@if (count($fixed_transactions))
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
			        <tr>
			          	<th>Nombre</th>
						<th>Tipo</th>
			          	<th>Categoria</th>
						<th>Monto</th>
						<th>Dia de pago</th>
			        	<th>Acción</th>
			        </tr>
		        </thead>
		        <tbody>
				@foreach ($fixed_transactions as $fixed_transaction)

					<tr>
						<td>{{ $fixed_transaction->title }}</td>
						<td>{{ $fixed_transaction->getType() }}</td>
						<td>{{ $fixed_transaction->category->title }}</td>
						<td>{{ $fixed_transaction->present()->amount_formatted }}</td>
						<td>{{ $fixed_transaction->payment_day }} de cada mes</td>
						<td>
							<a alt="Editar" title="Editar" href="{{ route('fixed-transactions.edit', $fixed_transaction) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
							<a alt="Desactivar" title="Desactivar" href="{{ route('fixed-transactions.deactivate', $fixed_transaction) }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						</td>
					</tr>

				@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="alert alert-warning" role="alert">No hay datos</div>
	@endif

@stop

@section('footer')

@stop