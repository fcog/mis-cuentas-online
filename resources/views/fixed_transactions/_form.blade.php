<div class="form-group">

    {!! Form::label('title', 'Título:') !!}
    {!! Form::text('title', null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('type_id', 'Tipo:') !!}
    {!! Form::select('type_id', $types, null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('category_id', 'Categoria:') !!}
    {!! Form::select('category_id', array(0 => 'Seleccione...') + $categories->toArray(), null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('amount', 'Monto:') !!}
    <div class="input-group">
        <span class="input-group-addon">$</span>
        {!! Form::input('text','amount', null, ['class'=>'form-control']) !!}
    </div>

</div>

<div class="form-group">

    {!! Form::label('payment_day', 'Dia de pago en el mes:') !!}
    {!! Form::input('number', 'payment_day', null, ['class'=>'form-control', 'min'=>1, 'max'=>31]) !!}

</div>

<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-success']) !!}

</div>

@section('javascript')
    <script>
        $( document ).ready(function() {

            var select1 = $("#type_id");

            select1.on('change', function(){

                var selectedvalue= select1.val();
                var url= "/api/categories/type/" + selectedvalue;

                $.getJSON(url, function(data){
                    var select2 = $('#category_id');
                    select2.empty();
                    select2.append($('<option></option>').html("Seleccione..."));
                    $.each(data, function(index, text) {
                        select2.append($('<option></option>').val(index).html(text));
                    });
                });
            });

            $( "#payment_day" ).datepicker();
            $( "#payment_day" ).datepicker( "option", "dateFormat", 'd' );
            $( "#payment_day" ).datepicker( 'setDate', new Date() );
            
        });
    </script>
@stop