@extends('layout')


@section('content')

    <div class="page-header">
        <h1>Editar una transacción fija</h1>
    </div>

    @include('errors._form_errors')

    <div class="well">

        {!! Form::model($fixed_transaction, ['method' => 'PATCH', 'action' => ['FixedTransactionsController@update', $fixed_transaction]]) !!}

            @include('fixed_transactions._form', ['submitButtonText' => 'Editar'])

        {!! Form::close() !!}

    </div>

@stop
