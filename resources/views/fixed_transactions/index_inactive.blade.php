@extends('layout')

@section('content')

    <div class="page-header">
        <h1>Transacciones Fijas inactivas</h1>
    </div>

    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Acciones</div>
        <div class="panel-body">
            <a class="btn btn-default" href="{{ route('fixed-transactions.index') }}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver activas</a>
        </div>
    </div>

    @if (count($fixed_transactions))
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Categoria</th>
                    <th>Monto</th>
                    <th>Dia de pago</th>
                    <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($fixed_transactions as $fixed_transaction)

                    <tr>
                        <td>{{ $fixed_transaction->title }}</td>
                        <td>{{ $fixed_transaction->getType() }}</td>
                        <td>{{ $fixed_transaction->category->title }}</td>
                        <td>{{ $fixed_transaction->present()->amount_formatted }}</td>
                        <td>{{ $fixed_transaction->payment_day }} de cada mes</td>
                        <td>
                            <a title="Activar" alt="Activar" href="{{ route('fixed-transactions.activate', $fixed_transaction) }}"><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
                            {!! Form::model($fixed_transaction, ['method' => 'DELETE', 'action' => ['FixedTransactionsController@destroy', $fixed_transaction], 'id' => 'delete' . $fixed_transaction->id, 'class' => 'delete']) !!}
                                <a href="javascript:;" onclick="document.getElementById('delete{{ $fixed_transaction->id }}').submit();" title="Borrar permanentemente"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                            {!! Form::close() !!}
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="alert alert-warning" role="alert">No hay datos</div>
    @endif

@stop

@section('footer')

@stop