@extends('layout')


@section('content')

	<div class="page-header">
		<h1>Crear Ingreso</h1>
	</div>

    @include('errors._form_errors')

	<div class="well">

		{!! Form::open(['url' => 'incomes']) !!}

			@include('incomes._form', ['submitButtonText' => 'Crear'])

		{!! Form::close() !!}

	</div>

@stop
