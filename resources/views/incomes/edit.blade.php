@extends('layout')


@section('content')

    <div class="page-header">
        <h1>Editar Ingreso</h1>
    </div>

    @include('errors._form_errors')

    <div class="well">

        {!! Form::model($transaction, ['method' => 'PATCH', 'action' => ['IncomesController@update', $transaction]]) !!}

            @include('incomes._form', ['submitButtonText' => 'Guardar'])

        {!! Form::close() !!}

    </div>

@stop
