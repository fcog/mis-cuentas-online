@extends('layout')
@section('content')

    <div class="page-header">
        <h1>Búsqueda</h1>
    </div>

    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Acciones</div>
        <div class="panel-body">

            {!! Form::open(['url' => '/', 'method' => 'GET', 'class' => 'navbar-form pull-right', 'role' => 'search']) !!}
                <div class="input-group">
                    {!! Form::input('text','search', null, ['class'=>'form-control', 'placeholder' => 'Buscar']) !!}
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            {!! Form::close() !!}

        </div>

    </div>

    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Resumen global</div>
        <div class="panel-body">
            <p><span class="label label-success">Ingresos:</span> ${{ number_format($balance['incomes'], 0, '.', '.') }} <span class="label label-danger">Gastos:</span> ${{ number_format($balance['expenses'], 0, '.', '.') }}</p>
        </div>
    </div>

    @if (count($transactions))

        @foreach ($transactions as $key => $transactions_month)

            <div class="page-header">
                <h3>{{ $key }}</h3>
            </div>

            <?php $monthly_balance = Transaction::calculateIncomesExpenses($transactions_month); ?>

            <div class="transactions-info">
                <span class="label label-success">Ingresos:</span> ${{ number_format($monthly_balance['incomes'], 0, '.', '.') }}
                <span class="label label-danger">Gastos:</span> ${{ number_format($monthly_balance['expenses'], 0, '.', '.') }}
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Categoria</th>
                        <th>Monto</th>
                        <th>Pagado con</th>
                        <th>Fecha de Pago</th>
                        <th>Fecha límite de pago</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach ($transactions_month as $transaction)

                            @if (is_null($transaction->paid_date))
                                @if($transaction->payment_date_limit &&
                                    ($transaction->payment_date_limit->diff(Date::now())->days < 3 || $transaction->payment_date_limit < Date::now()))
                                    <tr class="extreme-danger">
                                @else
                                    <tr class="warning">
                                @endif
                            @elseif ($transaction->type_id == config('constants.TYPE_INDEX.expense'))
                                <tr class="danger">
                            @elseif ($transaction->type_id == config('constants.TYPE_INDEX.income'))
                                <tr class="success">
                            @else
                                <tr>
                            @endif
                                <td>{{ $transaction->title }}</td>
                                <td>{{ $transaction->getType() }}</td>
                                <td>{{ $transaction->category->title }}</td>
                                <td>{{ $transaction->present()->amount_paid_formatted }}</td>
                                <td>{{ $transaction->getAccount() }}</td>
                                <td>{{ $transaction->present()->paid_date_formatted }}</td>
                                <td>{{ $transaction->present()->payment_date_limit_formatted }}</td>
                                <td> 
                                <?php switch ($transaction->type_id) {
                                    case config('constants.TYPE_INDEX.expense'):
                                        $controlador = 'expenses';
                                        break;
                                    case config('constants.TYPE_INDEX.income'):
                                        $controlador = 'incomes';
                                        break;                                    
                                    case config('constants.TYPE_INDEX.transfer'):
                                        $controlador = 'transfers';
                                        break; 
                                } ?>
                                @if ($transaction->type_id == config('constants.TYPE_INDEX.expense'))
                                    <a alt="Editar" title="Editar" href="{{ route('expenses.edit', $transaction) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    {!! Form::model($transaction, ['method' => 'DELETE', 'action' => ['ExpensesController@destroy', $transaction], 'id' => 'delete' . $transaction->id, 'class' => 'delete']) !!}
                                 
                                @elseif ($transaction->type_id == config('constants.TYPE_INDEX.income'))
                                    <a alt="Editar" title="Editar" href="{{ route('incomes.edit', $transaction) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>      
                                    {!! Form::model($transaction, ['method' => 'DELETE', 'action' => ['IncomesController@destroy', $transaction], 'id' => 'delete' . $transaction->id, 'class' => 'delete']) !!}
                                           
                                @elseif ($transaction->type_id == config('constants.TYPE_INDEX.transfer'))
                                    <a alt="Editar" title="Editar" href="{{ route('transfers.edit', $transaction) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                    {!! Form::model($transaction, ['method' => 'DELETE', 'action' => ['TransfersController@destroy', $transaction], 'id' => 'delete' . $transaction->id, 'class' => 'delete']) !!}
                                 
                                @endif
                                        <a href="javascript:;" onclick="document.getElementById('delete{{ $transaction->id }}').submit();" title="Borrar permanentemente"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    {!! Form::close() !!}  
                                </td>
                            </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>

        @endforeach
    @endif

@stop