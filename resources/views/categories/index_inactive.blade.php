@extends('layout')

@section('content')

	<div class="page-header">
		<h1>Categorias inactivas</h1>
	</div>

	<div class="panel panel-primary">
		<!-- Default panel contents -->
		<div class="panel-heading">Acciones</div>
		<div class="panel-body">
			<a class="btn btn-default" href="{{ route('categories.index') }}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver activas</a>
		</div>
	</div>

	@if (count($categories))
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
			        <tr>
			          <th>Nombre</th>
			          <th>Categoria</th>
			          <th>Acción</th>
			        </tr>
		        </thead>
		        <tbody>
				@foreach ($categories as $category)

					<tr>
						<td>{{ $category->title }}</td>
						<td>{{ $category->getType() }}</td>
						<td>
							<a title="Activar" alt="Activar" href="{{ route('categories.activate', $category) }}"><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
							{!! Form::model($category, ['method' => 'DELETE', 'action' => ['CategoriesController@destroy', $category], 'id' => 'delete']) !!}
								<a href="javascript:;" onclick="document.getElementById('delete').submit();" title="Borrar permanentemente"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
							{!! Form::close() !!}
						</td>
					</tr>

				@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="alert alert-warning" role="alert">No hay datos</div>
	@endif

@stop

@section('footer')

@stop