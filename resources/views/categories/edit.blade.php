@extends('layout')


@section('content')

	<div class="page-header">
		<h1>Editar una categoria</h1>
	</div>

	@include('errors._form_errors')

	<div class="well">

		{!! Form::model($category, ['method' => 'PATCH', 'action' => ['CategoriesController@update', $category]]) !!}

			@include('categories._form', ['submitButtonText' => 'Editar'])

		{!! Form::close() !!}

	</div>

@stop
